##############################################################################
# Lambda function to trigger training job based on S3 event
##############################################################################


# Import statement
import boto3
from datetime import datetime

# Global variables
sagemaker = boto3.client('sagemaker')

models = ["sagemaker-decision-trees"]  # List of models to be executed on a dataset
account_id = boto3.client('sts').get_caller_identity().get('Account')
region = boto3.session.Session().region_name
model_bucket = "optimusprime-sagemaker-models-us-east-1"

# Role to pass to SageMaker training job that has access to training data in S3, etc
sm_role = "arn:aws:iam::489394775236:role/service-role/AmazonSageMaker-ExecutionRole-20191004T103780"


def lambda_handler(event, context):
    key = event["Records"][0]["s3"]["object"]["key"]
    print(f"key: {key}")
    common_path = ("/").join(key.split("/")[:-1])
    bucket = event["Records"][0]["s3"]["bucket"]["name"]

    event_list = []

    for model in models:
        transform_output_folder = common_path
        s3_output_path = "s3://{}/{}".format(model_bucket, common_path)

        training_job_name = '{}'.format(model)
        train_manifest_uri = f"s3://{bucket}/{key}"
        container = f"{account_id}.dkr.ecr.{region}.amazonaws.com/{model}:latest"

        print(f'Starting training job for model: {model}')
        create_training_job(training_job_name, train_manifest_uri, container, s3_output_path)
        event['name'] = training_job_name
        event['container'] = container
        event['stage'] = 'Training'
        event['status'] = 'InProgress'
        event['message'] = 'Starting training job "{}"'.format(training_job_name)
        event_list.append(event)

    for i in event_list:
        print("------------------------------------------")
        print(i)
        print("------------------------------------------")


def create_training_job(name, train_manifest_uri, container, s3_output_path):
    """ Start SageMaker training job
    Args:
        name (string): Name to label training job with
        train_manifest_uri (string): URI to training data manifest file in S3
        container (string): Registry path of the Docker image that contains the training algorithm
        s3_output_path (string): Path of where in S3 bucket to output model artifacts after training
    Returns:
        (None)
    """
    now = datetime.now()
    time = now.strftime("%Y-%m-%d-%H-%M-%S")

    training_job_name = f"{name}-{time}" # to satisfy unique training job name
    try:
        response = sagemaker.create_training_job(
            TrainingJobName=training_job_name,
            AlgorithmSpecification={
                'TrainingImage': container,
                'TrainingInputMode': 'File'
            },
            RoleArn=sm_role,
            InputDataConfig=[
                {
                    'ChannelName': 'training',
                    'DataSource': {
                        'S3DataSource': {
                            'S3DataType': 'S3Prefix',
                            'S3Uri': train_manifest_uri,
                            'S3DataDistributionType': 'FullyReplicated'
                        }
                    },
                    'CompressionType': 'None'
                }
            ],
            OutputDataConfig={
                'S3OutputPath': s3_output_path
            },
            ResourceConfig={
                'InstanceType': "ml.c4.2xlarge",
                'InstanceCount': 1,
                'VolumeSizeInGB': 30
            },
            StoppingCondition={
                'MaxRuntimeInSeconds': 86400
            }
        )
    except Exception as e:
        print(e)
        print('Unable to create training job.')
        raise (e)
