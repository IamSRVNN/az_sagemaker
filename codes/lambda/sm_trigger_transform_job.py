##############################################################################
# Lambda function to create a model in sagemaker based on the training image
##############################################################################

# Import statement
import boto3

# Global variables
batch_job_output_bucket = "optimusprime-sagemaker-output-us-east-1"
input_data_bucket = "optimusprime-sagemaker-us-east-1"
sagemaker = boto3.client('sagemaker')
account_id = boto3.client('sts').get_caller_identity().get('Account')
region = boto3.session.Session().region_name
# Role to pass to SageMaker training job that has access to training data in S3, etc
sm_role = "arn:aws:iam::489394775236:role/service-role/AmazonSageMaker-ExecutionRole-20191004T103780"


def get_training_job_container(training_job_name):
    response = sagemaker.describe_training_job(
        TrainingJobName=training_job_name
    )
    return response['AlgorithmSpecification']['TrainingImage']


def create_model(model_name, image, model_url):
    model = sagemaker.create_model(
        ModelName=model_name,
        PrimaryContainer={
            'ContainerHostname': 'model-Container',
            'Image': image,
            'ModelDataUrl': model_url
        },
        ExecutionRoleArn=sm_role,
    )
    print(f"Create model response: {model}")


def create_transform_job(job_name, transform_data_url, transform_result_url):
    response = sagemaker.create_transform_job(
        TransformJobName=job_name,
        ModelName=job_name,
        TransformInput={
            'DataSource': {
                'S3DataSource': {
                    'S3DataType': 'S3Prefix',
                    'S3Uri': transform_data_url
                }
            },
            'ContentType': 'text/csv',
            'SplitType': 'Line'
        },
        TransformOutput={
            'S3OutputPath': transform_result_url,
            'Accept': 'text/csv',
            'AssembleWith': 'Line'
        },
        TransformResources={
            'InstanceType': 'ml.m4.xlarge',
            'InstanceCount': 1
        },
        DataProcessing={
            'InputFilter': '$[1:]',
        }
    )
    print(f"Transform job response: {response}")


def lambda_handler(event, context):
    # Create model in sagemaker
    key = event["Records"][0]["s3"]["object"]["key"]
    print(f"key: {key}")
    bucket = event["Records"][0]["s3"]["bucket"]["name"]
    training_job_name = key.split("/")[3]
    print(f"Training job name: {training_job_name}")
    training_job_image = get_training_job_container(training_job_name)
    print(f"Training container: {training_job_image}")
    model_url = f"s3://{bucket}/{key}"

    print("Creating model...")
    create_model(model_name=training_job_name,
                 image=training_job_image,
                 model_url=model_url)

    # Create batch transform job in sagemaker
    common_path =("/").join(key.split("/")[:3])
    input_data_url = f"s3://{input_data_bucket}/{common_path}/"
    output_data_url = f"s3://{batch_job_output_bucket}/{common_path}/"
    print("Creating transform job...")
    create_transform_job(job_name=training_job_name,
                         transform_data_url=input_data_url,
                         transform_result_url=output_data_url
                         )
    return True
