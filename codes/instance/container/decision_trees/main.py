import subprocess
import sys

argument = sys.argv[1]

if argument == 'train':
    cmd = "python /opt/program/train.py"
    process = subprocess.Popen(cmd.split(" "))
    process.wait()
if argument == 'serve':
    cmd = "python /opt/program/serve.py"
    process = subprocess.Popen(cmd.split(" "))
    process.wait()